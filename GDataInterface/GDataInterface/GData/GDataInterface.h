//
//  GDataInterface.h
//  GDataInterface
//
//  Some of the code in this class is from the original GData sample code, but it has been
//  enhanced somewhat and made to work on both iOS and MacOS transparently.
//
//  Created by Peter Easdown on 19/12/11.
//  Copyright (c) 2011 PKCLsoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#if TARGET_OS_IPHONE
#import "GDataDocs.h"
#import <UIKit/UIKit.h>
#else
#import "GData/GData.h"
#endif

@interface GDataInterfaceTypes

// This handler is used by methods that have no explicit result.  The boolean value indicates
// the success or failure of the the methods action.
//
typedef void (^CompletionHandler)(BOOL successful);

// This handler is called to update a progress indicator as a file is uploaded.
//
typedef void (^UploadProgressHandler)(double min, double max, double value);

// This handler is called to update a progress indicator as a file is downloaded.
//
typedef void (^DownloadProgressHandler)(double min, double max, double value);

@end

@interface GDataInterface : NSObject {
    
#if TARGET_OS_IPHONE
    // Needed so that when authenticating under iOS, we can push the google authentication
    // view, and later pop it.
    //
    UIViewController *rootController_;
#endif
    
    GDataFeedDocList *mDocListFeed;
    GDataServiceTicket *mDocListFetchTicket;
    NSError *mDocListFetchError;
    
    GDataFeedDocRevision *mRevisionFeed;
    GDataServiceTicket *mRevisionFetchTicket;
    NSError *mRevisionFetchError;
    
    GDataEntryDocListMetadata *mMetadataEntry;
    
    GDataServiceTicket *mUploadTicket;
    
    id uploadWindow;
    CompletionHandler uploadCompletionHandler;
    
    NSString *username_;
    NSString *password_;

}

// This handler is used when a list of documents has been requested.  The results parameter
// will be nil if the request failed.  If successful, then it will contain an array of 
// GDataEntryDocBase objects.
//
typedef void (^RetrievalCompletionHandler)(GDataFeedDocList* results, BOOL successful);

// This handler is used when a document has been downloaded.  If something prevented the 
// download from succeeding, then error parameter will be non-nil.
//
typedef void (^DocumentDownloadCompletionHandler)(NSData* fileContents, BOOL successful);

// Initializer that provides the username and password.
//
- (id) initWithUsername:(NSString*)username andPassword:(NSString*)password;

// Returns the shared instance of the class.  There will only ever be a single instance
// of this class.
//
+ (GDataInterface*) sharedInstance;

// Returns YES if currently signed in.
//
- (BOOL) isSignedIn;

// Signs in or out depending on current state, and executes the options completion handler
// block.  The window parameter is used to specify the root viewController object used when
// displaying login windows via GData, or error dialogs.
//
- (void) signInOrOutWithCompletionHandler:(CompletionHandler)handler forWindow:(id)window;

// Will retrieve a list of documents using the cached connection, and call the specified
// handler block, providing the list of documents, and a success/fail indication.
//
- (void) retrieveDocumentListWithCompletionHandler:(RetrievalCompletionHandler)handler;

// Will download the file at the specified URL.  This is not Google Docs specific and will work
// for any URL.  Be careful not to try and retrieve large files and the result is stored
// in memory.
//
- (void) downloadURL:(NSURL*)url withProgressHandler:(DownloadProgressHandler)progressHandler andCompletionHandler:(DocumentDownloadCompletionHandler)handler;

// Will download the specified google docs document.
//
- (void) downloadDocument:(GDataEntryDocBase*)document withProgressHandler:(DownloadProgressHandler)progressHandler andCompletionHandler:(DocumentDownloadCompletionHandler)handler;

// Uploads the document entry, optionally updating it with a new revision.
//
- (void) uploadEntry:(GDataEntryDocBase*)docEntry asNewRevision:(BOOL)newRevision forWindow:(id)window withProgressHandler:(UploadProgressHandler)progressHandler andCompletionHandler:(CompletionHandler)handler;

// Uploads the specified file to the authenticated google docs account.
//
- (void)uploadFileAtPath:(NSString *)path forWindow:(id)window withProgressHandler:(UploadProgressHandler)progressHandler andCompletionHandler:(CompletionHandler)handler;

// More for internal use than anything else.  Used to determine the mime type based on the google docs class
// and/or file extension.
//
- (void)getMIMEType:(NSString **)mimeType andEntryClass:(Class *)class forExtension:(NSString *)extension;

// Getter and Setter for username,
//
- (void) setUsername:(NSString*)newUsername;
- (NSString*) username;

// Getter and Setter for password.  The password will be encrypted before storing it in user preferances.
//
- (void) setPassword:(NSString*)newPassword;
- (NSString*) password;

// Returns the username that google is given for signing in.
//
- (NSString *)signedInUsername;

// Returns a static instance of the docs service.
//
+ (GDataServiceGoogleDocs *)docsService;

@end
